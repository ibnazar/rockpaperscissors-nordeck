##Introduction

This project is basically implementation of widely known rockpaperscissors game. Tools and technologies used while creating this project are; JAVA, Maven, Junit, IntelliJ, BitBucket

##Project Structure

It contains 4 basic files, which are defined down;

**Player.java**

This file contains information about user and choices about the user, it's but a simple pojo class

**Game.java**

This is controller or logic class of the game which implements Gameable interface and contains information about the rules of the game (specific to this game), contains functionalities of determining a winner,
assigning the moves to the players and in the end showing the results. This class contains a method named generateRules which is scalable and we can add many rules to this game and enhance the functionality of the 
game without changing the logic about the results/winners or results.

**Gameable.java** 

This interface includes the contract which can be used for the new classes when created to improve on the already built functionality of the game.

**Main.java**

This is entry point of the game, which starts the game without exposing any business logic of the game.


##Design Choices

During implementation I had to take a few decisions which is why the code has turned out so long, in the end the idea was to make the code as simple as possible and as scalable and reusable as possible.

**Why game rules were defined in a different method rather than simply using multiple if conditions**

Since all the posibilities of the combinations were already known I could have used simple ifs and could have done it in a few lines rather than writing generateRules and computeResult methods but I didn't do it.
When we use multiple if else conditions we increase the cyclometic complexity for the project and I would have to write multiple test cases to verify all the if else cases thus making the code look rather horrible
or not as per standards. While when we would have added new rules we would have to change the conditions and add new conditions and test cases not making the code reusable. 

**Game rules using enums rather than HashMap**
We can also implement the logic of rules in enums and then create map with the enums, which can be done using following.

Create an Enum


public enum Figures {
    rock, paper, scissors
}


Change generateRules to 


private Map<Figures, String> generateRules() {
        rules = new EnumMap<Figures, String>(Figures.class);
        rules.put(Figures.rock, "scissors");
        rules.put(Figures.paper, "rock");
        rules.put(Figures.scissors, "paper");
        return rules;
    }
	

and access the value from it using


String weakFigure = rules.get(Figures.valueOf(player1Move));
 
**Why not all the methods were put in the interfaces**

Interfaces are used as a contract between different classes when we add new class we need common methods from the interface so that we can added our personal methods or choices on our own. Since the game can be used
by others so we only need the methods those are going to be used also by others not all. For example new game can have its own rules so generateRules and computer results are not mentioned in the interfaces. 

**Why Maven**

Since I've used Junit5 I would have to download dependencies on my own if I have not used the build tool, now I just have to inject the dependency details in the pom.xml and maven takes care of managing the dependency
problems on its own.