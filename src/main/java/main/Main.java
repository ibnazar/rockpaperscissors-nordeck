/*
 *
 * This class is the main entry point of the project, it also does not contain any informationfullfils the basic fields needed for the player and also the choices it can make
 * about the implementation of the code
 *
 * */

package main;

import com.rockscissorpaper.controllers.Game;

public class Main {

    public static void main(String[] args){
        Game game = new Game();
        game.startGame();
    }
}
