/*
*
* This class fullfils the basic fields needed for the player and also the choices it can make
* It contains different fields and their setter getters, simply a pojo
*
* */

package com.rockscissorpaper.beans;

public class Player {

    private int totalWins;
    private String[] figures = {"rock", "paper", "scissors"};


    public int getTotalWins() {
        return totalWins;
    }

    public void increaseWin() {
        totalWins++;
    }

    public String[] getFigures(){
        return figures;
    }

    public String makeMove(int index){
        return figures[index];
    }



}
