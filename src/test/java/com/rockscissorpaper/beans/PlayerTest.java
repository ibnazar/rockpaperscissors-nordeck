package com.rockscissorpaper.beans;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlayerTest {

    Player playerObject;

    @BeforeEach
    public void setup() {
        playerObject = new Player();
    }

    @Test
    public void test_makeMove() {
        assertEquals(playerObject.getFigures()[1], playerObject.makeMove(1));
    }

}