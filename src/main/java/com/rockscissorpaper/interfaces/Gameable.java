/*
 *
 * Gameable intefaces defines the contract about the contract meaning which methods it has to
 * implement
 *
 * */

package com.rockscissorpaper.interfaces;

public interface Gameable {

    public void startGame();
    public void executeGame();

}
