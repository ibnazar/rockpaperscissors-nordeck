/*

This class is the main logic of the game implements Gameable interface
generateRules method can be scaled by just adding other rules and providing their weak and strong points
computeResults gets choices of the players and decides the winner or draw
showResult gives desired results

 */

package com.rockscissorpaper.controllers;

import com.rockscissorpaper.beans.Player;
import com.rockscissorpaper.interfaces.Gameable;

import java.util.*;

public class Game implements Gameable {

    private int undecidedGames;

    private final int MAX_GAMES = 10;

    private Player player1;
    private Player player2;

    private HashMap<String, String> rules;

    public Game() {
        rules = this.generateRules();
        player1 = new Player();
        player2 = new Player();
    }

    /*
     * Run the game and show the results
     * */
    public void startGame() {
        executeGame();
        showResult();
    }


    public void executeGame() {

        Random randomObj = new Random();

        for(int gameCounter = 1; gameCounter <= MAX_GAMES; gameCounter++) {
            String player1Move = player1.makeMove(randomObj.nextInt(3));
            String player2Move = player2.makeMove(randomObj.nextInt(3));
            computeResult(player1Move,player2Move);
        }
    }


    /*
     * Rules about the game, key means stronger of both and value means weaker
     * */
    private HashMap<String, String> generateRules() {
        rules = new HashMap<String, String>();
        rules.put("rock", "scissors");
        rules.put("paper", "rock");
        rules.put("scissors", "paper");
        return rules;
    }


    /*
     * Compute the results, and update the player objects accordingly
     * */
    public void computeResult(String player1Move, String player2Move) {

        if(player1Move.equals(player2Move)) {
            undecidedGames++;
            return;
        }

        String weakFigure = rules.get(player1Move);

        if(weakFigure != null && weakFigure.isEmpty()) return;

        if(player2Move.equals(weakFigure)) {
            player1.increaseWin();
        }else {
            player2.increaseWin();
        }
    }


    /*
     * Results to show on the console
     * */
    private void showResult() {
        System.out.println(String.format("Number of games played: %d", MAX_GAMES));
        System.out.println(String.format("Victory Player 1: %d", player1.getTotalWins()));
        System.out.println(String.format("Siege Player 2: %d", player2.getTotalWins()));
        System.out.println(String.format("Undecided Games: %d", undecidedGames));
    }

    public int getUndecidedGames() {
        return undecidedGames;
    }

    public Player getPlayer1() {
        return player1;
    }

    public Player getPlayer2() {
        return player2;
    }

    public HashMap<String, String> getRules() {
        return rules;
    }
}
