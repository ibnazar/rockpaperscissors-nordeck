/*
*
* Purpose of this file is to test the main functionality of the game
*
* */

package com.rockscissorpaper.controllers;

import com.rockscissorpaper.beans.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class GameTest {

    private Game game;

    @BeforeEach
    public void setup() {
        game = new Game();
    }

    @Test
    public void test_executeGame() {
        final int MAX_GAMES = 10;

        game.executeGame();

        Player player1 = game.getPlayer1();
        Player player2 = game.getPlayer2();

        int p1TotalWins = player1.getTotalWins();
        int p2TotalWins = player2.getTotalWins();

        int undecidedGames = game.getUndecidedGames();

        int attemptedGames = p1TotalWins + p2TotalWins + undecidedGames; //Should be equal to 10

        assertEquals(attemptedGames, MAX_GAMES);
    }

    @Test
    public void test_computeResult_player1Win() {
        String player1Move = "rock";
        String player2Move = "scissors";

        game.computeResult(player1Move, player2Move);

        int p1TotalWins = game.getPlayer1().getTotalWins();
        int p2TotalWins = game.getPlayer2().getTotalWins();

        assertEquals(p1TotalWins, 1);
        assertEquals(p2TotalWins, 0);
    }

    @Test
    public void test_computeResult_player2Win() {
        String player1Move = "scissors";
        String player2Move = "rock";

        game.computeResult(player1Move, player2Move);

        int p1TotalWins = game.getPlayer1().getTotalWins();
        int p2TotalWins = game.getPlayer2().getTotalWins();

        assertEquals(p1TotalWins, 0);
        assertEquals(p2TotalWins, 1);
    }

    @Test
    public void test_UndecidedGames() {
        String player1Move = "scissors";
        String player2Move = "scissors";

        game.computeResult(player1Move, player2Move);

        int p1TotalWins = game.getPlayer1().getTotalWins();
        int p2TotalWins = game.getPlayer2().getTotalWins();

        assertEquals(p1TotalWins, 0);
        assertEquals(p2TotalWins, 0);
        assertEquals(game.getUndecidedGames(), 1);
    }

    @Test
    public void test_generateRules() {
        HashMap<String, String> rules = game.getRules();

        assertNotNull(rules);
        assertTrue(rules.containsKey("rock"));
    }
}